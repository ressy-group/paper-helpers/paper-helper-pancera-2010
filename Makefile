panels = S2A S2B
s2out = $(addsuffix .fa,$(addprefix parsed/,$(panels)))

.SECONDARY:

all: output/seqs.csv

output/seqs.csv: $(s2out)
	python scripts/combine_s2.py $^ $@

parsed/%.fa: from-paper/fig%.txt
	python scripts/parse_s2.py $^ $@

# PG9 and PG16 seqs from Pancera 2010

Mature and reverted amino acid sequences for PG9 and PG16 antibodies from:

Marie Pancera et al.
Crystal Structure of PG16 and Chimeric Dissection with Somatically Related PG9: Structure-Function Analysis of Two Quaternary-Specific Antibodies That Effectively Neutralize HIV-1.
Journal of Virology, Volume 84, No 16, 8098 – 8110
<https://doi.org/10.1128/jvi.00966-10>

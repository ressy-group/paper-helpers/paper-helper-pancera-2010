#!/usr/bin/env python

"""
Make combined CSV of sequences from Figure S2.
"""

import re
import sys
from csv import DictWriter
from collections import defaultdict

def seqdict(path):
    seqs = defaultdict(str)
    seqid = None
    with open(path) as f_in:
        for line in f_in:
            line = line.strip()
            if line.startswith(">"):
                seqid = line[1:]
            else:
                seqs[seqid] += line
    return seqs

def combine_s2(paths_in, path_out):
    entries = defaultdict(dict)
    for path in paths_in:
        for seqid, seq in seqdict(path).items():
            match = re.match("(PG[0-9]+)V([hl])reverted", seqid)
            if match:
                mab = match.group(1)
                chain = "Heavy" if match.group(2) == "h" else "Light"
                cat = "reverted"
            else:
                match = re.match("(PG[0-9]+)(Heavy|Light)", seqid)
                mab = match.group(1)
                chain = match.group(2)
                cat = "mature"
            entries[(mab, cat)]["Antibody"] = mab
            entries[(mab, cat)]["Category"] = cat
            entries[(mab, cat)][f"{chain}AA"] = re.sub("-", "", seq)
    entries = list(entries.values())
    entries.sort(key=lambda x: (x["Antibody"], x["Category"]))
    with open(path_out, "w") as f_out:
        writer = DictWriter(
            f_out, ["Antibody", "Category", "HeavyAA", "LightAA"], lineterminator="\n")
        writer.writeheader()
        writer.writerows(entries)

if __name__ == "__main__":
    combine_s2(sys.argv[1:-1], sys.argv[-1])
